# Use LTS for base images
FROM ubuntu:bionic

# Avoid questions from packages during configuration
ENV DEBIAN_FRONTEND noninteractive

# NOTE: mutliple apt runs create extra layers!
RUN apt-get update && apt-get install -y \
    wget \
    apache2 \
    php7.2 \
    libapache2-mod-php7.2 \
    php7.2-common \
    php7.2-mbstring \
    php7.2-xmlrpc \
    php7.2-gd \
    php7.2-xml \
    php7.2-mysql \
    php7.2-cli \
    php7.2-zip \
    php7.2-curl
